<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 250)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('default_image', 250)->nullable();
			$table->string('left_image', 250)->nullable();
			$table->string('right_image', 250)->nullable();
			$table->timestamps();
			$table->string('slug', 191);
			$table->softDeletes();
			$table->string('code', 191);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
