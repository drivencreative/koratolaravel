<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSeriesTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('series_translations', function(Blueprint $table)
		{
			$table->foreign('series_id')->references('id')->on('series')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('series_translations', function(Blueprint $table)
		{
			$table->dropForeign('series_translations_series_id_foreign');
		});
	}

}
