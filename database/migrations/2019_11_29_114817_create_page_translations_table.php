<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_translations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('page_id')->unsigned()->nullable();
			$table->string('title', 191);
			$table->text('excerpt', 65535)->nullable();
			$table->text('body', 65535)->nullable();
			$table->string('image', 191)->nullable();
			$table->string('slug', 191)->unique();
			$table->text('meta_description', 65535)->nullable();
			$table->text('meta_keywords', 65535)->nullable();
			$table->string('locale', 191)->index();
			$table->unique(['page_id','locale']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('page_translations');
	}

}
