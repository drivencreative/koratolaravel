<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('image', 191)->nullable();
			$table->text('specification', 65535)->nullable();
			$table->decimal('price_eu', 10, 0)->nullable();
			$table->decimal('price_rsd', 10, 0)->nullable();
			$table->text('price_description', 65535)->nullable();
			$table->timestamps();
			$table->string('slug')->nullable();
			$table->softDeletes();
			$table->smallInteger('is_new')->default(0);
			$table->string('series_id', 191)->nullable();
			$table->string('code', 191);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
