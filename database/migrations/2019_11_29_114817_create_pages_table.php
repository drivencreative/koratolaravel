<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 191);
			$table->text('body', 65535)->nullable();
			$table->string('image', 191)->nullable();
			$table->string('slug', 191)->unique();
			$table->text('meta_description', 65535)->nullable();
			$table->text('meta_keywords', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('page_id')->unsigned()->nullable()->index('pages_page_uid_index');
			$table->boolean('status')->default(0);
			$table->string('code', 191);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
