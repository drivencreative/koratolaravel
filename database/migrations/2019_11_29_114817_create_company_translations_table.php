<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_translations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->unsigned();
			$table->string('name', 191);
			$table->string('locale', 191)->index();
			$table->text('description', 65535)->nullable();
			$table->string('slug', 191);
			$table->timestamps();
			$table->unique(['locale','slug'], 'company_translations_slug_uindex');
			$table->unique(['company_id','locale']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_translations');
	}

}
