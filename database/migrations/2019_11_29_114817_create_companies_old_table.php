<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesOldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies_old', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('name', 200);
			$table->string('short_name', 100);
			$table->integer('description');
			$table->integer('short_description');
			$table->string('default_baner_left', 300);
			$table->string('default_baner_right', 300);
			$table->string('footer_image', 300);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies_old');
	}

}
