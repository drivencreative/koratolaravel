<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_translations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('news_id')->unsigned();
			$table->string('locale', 191)->index();
			$table->string('title', 191);
			$table->text('text', 65535)->nullable();
			$table->string('slug', 191)->unique();
			$table->timestamps();
			$table->unique(['news_id','locale']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news_translations');
	}

}
