<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeriesTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('series_translations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('series_id')->unsigned();
			$table->string('name', 191);
			$table->string('locale', 191)->index();
			$table->text('description', 65535)->nullable();
			$table->string('slug', 191);
			$table->timestamps();
			$table->unique(['series_id','locale']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('series_translations');
	}

}
