<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsOldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products_old', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 300);
			$table->string('name_srp', 50)->unique('short_name');
			$table->text('description_eng', 65535);
			$table->text('description_srp', 65535);
			$table->string('short_description', 500);
			$table->string('default_image', 100)->default('products/');
			$table->string('small_image', 100);
			$table->string('default_banner', 100)->nullable();
			$table->binary('new_product', 1)->default(0);
			$table->integer('series_id')->default(0);
			$table->text('specifications_srp', 65535);
			$table->text('specifications_eng', 65535);
			$table->integer('sort_field')->nullable();
			$table->string('banner_image', 200)->nullable();
			$table->string('banner_description_eng', 300)->nullable();
			$table->string('banner_description_srp', 300)->nullable();
			$table->integer('price_eur')->nullable();
			$table->integer('price_din')->nullable();
			$table->string('price_description_eng', 200)->nullable();
			$table->string('price_description_srp', 200)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products_old');
	}

}
