<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_translations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->string('locale', 191)->index();
			$table->string('name', 191);
			$table->text('description', 65535)->nullable();
			$table->text('specification', 65535)->nullable();
			$table->text('price_description', 65535)->nullable();
			$table->string('slug', 191);
			$table->timestamps();
			$table->unique(['product_id','locale']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_translations');
	}

}
