<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeriesMigrationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('series_migration', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 300);
			$table->string('name_srp', 200)->index('short_name');
			$table->text('description_eng', 65535);
			$table->text('description_srp', 65535)->nullable();
			$table->integer('short_description');
			$table->string('default_image', 100)->default('0');
			$table->string('default_banner', 100);
			$table->integer('distributor_id');
			$table->integer('sort_field')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('series_migration');
	}

}
