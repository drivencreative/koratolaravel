<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('setting_translations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('setting_id')->unsigned();
			$table->string('locale', 191)->index();
			$table->string('key', 191);
			$table->string('display_name', 191);
			$table->string('value', 191);
			$table->unique(['setting_id','locale']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('setting_translations');
	}

}
