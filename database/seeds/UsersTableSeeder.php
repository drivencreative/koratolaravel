<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {

            User::create([
                'name'           => 'Dragan Radisic',
                'email'          => 'scorrp@gmail.com',
                'password'       => bcrypt('35koratoAdmin'),
                'remember_token' => str_random(60),
            ]);
        }
    }
}
