$(document).ready(function () {
	$('.file-upload-switch').change(function () {
		$('.form-control.value').prop({type: "text"})
		if($(this).is(":checked")) {
			$('.form-control.value').prop({type: "file"})
		}
	});
	if($('.file-upload-switch:checkbox').is(':checked')) {
		$('.form-control.value').prop({type: "file"})
	}
});