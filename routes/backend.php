<?php

Route::group(['prefix' => 'backend', 'middleware' => 'auth'], function () {

//  Dashboard
    Route::get('/', [
        'uses' => 'PageController@index',
        'as' => 'dashboard'
    ]);
//  Page
    Route::group([
        'prefix' => 'pages'
    ], function () {
        Route::get('/', [
            'uses' => 'PageController@index',
            'as' => 'page.index'
        ]);
        Route::get('/trashed', [
            'uses' => 'PageController@trashed',
            'as' => 'page.trashed'
        ]);
        Route::get('/kill/{id}', [
            'uses' => 'PageController@kill',
            'as' => 'page.kill'
        ]);
        Route::get('/restore/{id}', [
            'uses' => 'PageController@restore',
            'as' => 'page.restore'
        ]);
        Route::get('/create', [
            'uses' => 'PageController@create',
            'as' => 'page.create'
        ]);
        Route::post('/store', [
            'uses' => 'PageController@store',
            'as' => 'page.store'
        ]);
        Route::post('/update/{id}', [
            'uses' => 'PageController@update',
            'as' => 'page.update'
        ]);
        Route::get('/edit/{id}', [
            'uses' => 'PageController@edit',
            'as' => 'page.edit'
        ]);
        Route::get('/delete/{id}', [
            'uses' => 'PageController@destroy',
            'as' => 'page.delete'
        ]);

    });

//    Product
    Route::group([
        'prefix' => 'products'
    ], function () {
        Route::get('/', [
            'uses' => 'ProductController@index',
            'as' => 'product.index'
        ]);
        Route::get('/trashed', [
            'uses' => 'ProductController@trashed',
            'as' => 'product.trashed'
        ]);
        Route::get('/kill/{id}', [
            'uses' => 'ProductController@kill',
            'as' => 'product.kill'
        ]);
        Route::get('/restore/{id}', [
            'uses' => 'ProductController@restore',
            'as' => 'product.restore'
        ]);
        Route::get('/create', [
            'uses' => 'ProductController@create',
            'as' => 'product.create'
        ]);
        Route::post('/store', [
            'uses' => 'ProductController@store',
            'as' => 'product.store'
        ]);
        Route::post('/update/{id}', [
            'uses' => 'ProductController@update',
            'as' => 'product.update'
        ]);
        Route::get('/edit/{id}', [
            'uses' => 'ProductController@edit',
            'as' => 'product.edit'
        ]);
        Route::get('/delete/{id}', [
            'uses' => 'ProductController@destroy',
            'as' => 'product.delete'
        ]);
    });

    // Company
    Route::group([
        'prefix' => 'companies'
    ], function () {
        Route::get('/', [
            'uses' => 'CompanyController@index',
            'as' => 'companies.index'
        ]);
        Route::get('/trashed', [
            'uses' => 'CompanyController@trashed',
            'as' => 'companies.trashed'
        ]);
        Route::get('/kill/{id}', [
            'uses' => 'CompanyController@kill',
            'as' => 'companies.kill'
        ]);
        Route::get('/restore/{id}', [
            'uses' => 'CompanyController@restore',
            'as' => 'companies.restore'
        ]);
        Route::get('/create', [
            'uses' => 'CompanyController@create',
            'as' => 'companies.create'
        ]);
        Route::post('/store', [
            'uses' => 'CompanyController@store',
            'as' => 'companies.store'
        ]);
        Route::post('/update/{id}', [
            'uses' => 'CompanyController@update',
            'as' => 'companies.update'
        ]);
        Route::get('/edit/{id}', [
            'uses' => 'CompanyController@edit',
            'as' => 'companies.edit'
        ]);
        Route::get('/delete/{id}', [
            'uses' => 'CompanyController@destroy',
            'as' => 'companies.delete'
        ]);
    });

    // Series
    Route::group([
        'prefix' => 'series'
    ], function () {
        Route::get('/', [
            'uses' => 'SeriesController@index',
            'as' => 'series.index'
        ]);
        Route::get('/trashed', [
            'uses' => 'SeriesController@trashed',
            'as' => 'series.trashed'
        ]);
        Route::get('/kill/{id}', [
            'uses' => 'SeriesController@kill',
            'as' => 'series.kill'
        ]);
        Route::get('/restore/{id}', [
            'uses' => 'SeriesController@restore',
            'as' => 'series.restore'
        ]);
        Route::get('/create', [
            'uses' => 'SeriesController@create',
            'as' => 'series.create'
        ]);
        Route::post('/store', [
            'uses' => 'SeriesController@store',
            'as' => 'series.store'
        ]);
        Route::post('/update/{id}', [
            'uses' => 'SeriesController@update',
            'as' => 'series.update'
        ]);
        Route::get('/edit/{id}', [
            'uses' => 'SeriesController@edit',
            'as' => 'series.edit'
        ]);
        Route::get('/delete/{id}', [
            'uses' => 'SeriesController@destroy',
            'as' => 'series.delete'
        ]);
    });

    // News
    Route::group([
        'prefix' => 'news'
    ], function () {
        Route::get('/', [
            'uses' => 'NewsController@index',
            'as' => 'news.index'
        ]);
        Route::get('/trashed', [
            'uses' => 'NewsController@trashed',
            'as' => 'news.trashed'
        ]);
        Route::get('/kill/{id}', [
            'uses' => 'NewsController@kill',
            'as' => 'news.kill'
        ]);
        Route::get('/restore/{id}', [
            'uses' => 'NewsController@restore',
            'as' => 'news.restore'
        ]);
        Route::get('/create', [
            'uses' => 'NewsController@create',
            'as' => 'news.create'
        ]);
        Route::post('/store', [
            'uses' => 'NewsController@store',
            'as' => 'news.store'
        ]);
        Route::post('/update/{id}', [
            'uses' => 'NewsController@update',
            'as' => 'news.update'
        ]);
        Route::get('/edit/{id}', [
            'uses' => 'NewsController@edit',
            'as' => 'news.edit'
        ]);
        Route::get('/delete/{id}', [
            'uses' => 'NewsController@destroy',
            'as' => 'news.delete'
        ]);
    });

    // Settings
    Route::group([
        'prefix' => 'settings'
    ], function () {
        Route::get('/', [
            'uses' => 'SettingsController@index',
            'as' => 'setting.index'
        ]);
        Route::post('/update/{id}', [
            'uses' => 'SettingsController@update',
            'as' => 'setting.update'
        ]);
        Route::get('/edit/{id}', [
            'uses' => 'SettingsController@edit',
            'as' => 'setting.edit'
        ]);
        Route::get('/delete/{id}', [
            'uses' => 'SettingsController@destroy',
            'as' => 'setting.delete'
        ]);
        Route::get('/create', [
            'uses' => 'SettingsController@create',
            'as' => 'setting.create'
        ]);
        Route::post('/store', [
            'uses' => 'SettingsController@store',
            'as' => 'setting.store'
        ]);
        Route::get('/profile', [
            'uses' => 'SettingsController@profile',
            'as' => 'setting.profile'
        ]);
    });
});