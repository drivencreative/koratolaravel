<?php


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Auth::routes();


// Main GET routes with locale
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localize' ] // Route translate middleware
], function () {
    Route::get(LaravelLocalization::transRoute('routes.index'), [
        'uses' => 'Controller@index',
        'as' => 'index'
    ]);

    Route::get(LaravelLocalization::transRoute('routes.companies'), [
        'uses' => 'CompanyController@show',
        'as' => 'company'
    ])->where('slug', '([A-Za-z0-9\-\/]+)');

    Route::get(LaravelLocalization::transRoute('routes.product'), [
        'uses' => 'ProductController@show',
        'as' => 'product'
    ])->where('slug', '([A-Za-z0-9\-\/]+)');

    Route::get(LaravelLocalization::transRoute('routes.series'), [
        'uses' => 'SeriesController@show',
        'as' => 'series'
    ])->where('slug', '([A-Za-z0-9\-\/]+)');

//    Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);

    Route::get(LaravelLocalization::transRoute('routes.page'), [
        'uses' => 'PageController@show',
        'as' => 'page'
    ])->where('slug', '([A-Za-z0-9\-\/]+)');

//
//
//    Route::get('/', 'Controller@index', [
//        'uses' => 'Controller@index',
//        'as' => 'index'
//    ]);
//
//    Route::get('/company/{slug}', [
//        'uses' => 'CompanyController@show',
//        'as' => 'company'
//    ])->where('slug', '([A-Za-z0-9\-\/]+)');
//
//    Route::get('/product/{slug}', [
//        'uses' => 'ProductController@show',
//        'as' => 'product'
//    ])->where('slug', '([A-Za-z0-9\-\/]+)');
//
//    Route::get('/series/{slug}', [
//        'uses' => 'SeriesController@show',
//        'as' => 'series'
//    ])->where('slug', '([A-Za-z0-9\-\/]+)');
//
//
//    Route::get('{slug}', [
//        'uses' => 'PageController@show',
//        'as' => 'page'
//    ])->where('slug', '([A-Za-z0-9\-\/]+)');

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
