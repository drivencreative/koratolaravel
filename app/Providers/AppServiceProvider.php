<?php

namespace App\Providers;

use App\Settings;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->share('settings', Settings::all());
        view()->share('keywords', Settings::where('key', 'site.keywords')->first()['value'] ?? 'Uvoznik i distributer Hi-Fi, audio i video opreme');
        view()->share('title', Settings::where('key', 'site.title')->first()['value'] ?? '');
        view()->share('topRightImage', Settings::where('key', 'site.top_right_image')->first()['value'] ?? '');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Controller::class, ExtendedController::class);
    }

}
