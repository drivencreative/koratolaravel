<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model implements TranslatableContract
{
    use SoftDeletes, Translatable;

    public $translatedAttributes = ['title', 'body', 'meta_description', 'meta_keywords', 'slug'];
    public $translationModel = PageTranslation::class;
    protected $with = ['translations'];
    /**
     * @var array
     */
    protected $fillable = ['title', 'body', 'status', 'image', 'meta_description', 'meta_keywords', 'slug', 'page_uid', 'code'];

    /**
     * Get the user that owns the phone.
     */
    public function parent()
    {
        return $this->belongsTo(Page::class, 'page_id');
    }

    /**
     * Get the user that owns the phone.
     */
    public function child()
    {
        return $this->hasMany(Page::class, 'page_id');
    }
}
