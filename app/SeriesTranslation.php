<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeriesTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'description', 'slug'];

    public function original(){
        return $this->hasOne('App\Series', 'id', 'series_id')->first();
    }
}
