<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'description', 'specification', 'price_description', 'slug'];

    public function original(){
        return $this->hasOne('App\Product', 'id', 'product_id')->first();
    }
}
