<?php

namespace App;

abstract class Model extends \Illuminate\Database\Eloquent\Model
{

    /**
     * @param $slug
     * @return mixed
     */
    public static function getBySlug($slug)
    {
        return self::where('slug', $slug)->first();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function localizeModel($slug){
        return self::getBySlug($slug);
    }
}