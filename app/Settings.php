<?php

namespace App;


class Settings extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'key', 'display_name', 'value', 'details', 'type'
    ];
}
