<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'description', 'slug'];

    public function original(){
        return $this->hasOne('App\Company', 'id', 'company_id')->first();
    }


    /**
     * Get the series
     */
    public function series()
    {
        return $this->hasMany(Series::class, 'id', 'series_id');

    }
}
