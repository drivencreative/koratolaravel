<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model implements TranslatableContract
{
    use SoftDeletes, Translatable;

    public $translatedAttributes = ['name', 'description', 'specification', 'price_description', 'slug'];
    public $translationModel = ProductTranslation::class;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'is_new', 'image', 'specification', 'price_eu', 'price_rsd', 'price_description', 'slug', 'series_id', 'code'
    ];

    public function series()
    {
        return $this->belongsTo(\App\Series::class, 'series_id');
    }
}
