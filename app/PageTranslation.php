<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{

    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'status', 'image', 'meta_description', 'meta_keywords', 'slug', 'page_uid', 'code'
    ];

    public function original(){
        return $this->hasOne('App\Page', 'id', 'page_id')->first();
    }

    public static function getBySlug($slug)
    {
        return self::where('slug',$slug)->first();
    }
}
