<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'text', 'slug'];

    public function original(){
        return $this->hasOne('App\News', 'id', 'page_id')->first();
    }
}
