<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model implements TranslatableContract
{
    use SoftDeletes, Translatable;

    public $translatedAttributes = ['title', 'text', 'slug'];
    public static $translationModel = NewsTranslation::class;

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'text', 'image', 'slug', 'code'
    ];
}
