<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Company extends Model implements TranslatableContract
{
    use SoftDeletes, Translatable;

    public $translatedAttributes = ['name', 'description', 'slug'];
    public static $translationModel = CompanyTranslation::class;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'default_image', 'left_image', 'right_image', 'slug', 'code'
    ];

    /**
     * Get the series
     */
    public function series()
    {
        return $this->hasMany(Series::class);

    }
}