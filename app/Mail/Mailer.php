<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mailer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    public $mailer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('milosradisic@raleservice.com')
            ->view('mails.mail')
            ->text('mails.mail_plain')
            ->with(
                [
                    'testVarOne' => '1',
                    'testVarTwo' => '2',
                ])
            ->attach(public_path('/img').'/contact-bg.jpg', [
                'as' => 'contact-bg.jpg',
                'mime' => 'image/jpeg'
            ]);
    }
}
