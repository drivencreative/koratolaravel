<?php

namespace App\Http\Controllers;

use App\Company;
use App\Product;
use Illuminate\Support\Facades\DB;
use Session;
use Request as Req;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $company = Company::localizeModel($slug);
        if (!$company) {
            return \App::abort(404);
        }
        $randomCompanies = Company::all()->random(3);
        return view('company',
            [
                'randomCompanies' => $randomCompanies,
                'randomProducts' => Product::all()->random(3),
                'news' => 0,
                'company' => $company,
                'title' => $company->name,
            ]
        );
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Req::get('search')) {

            // Perform the query using Query Builder
            $result = DB::table('companies')
                ->select(DB::raw("*"))
                ->where('name', 'LIKE', '%' . Req::get('search') . '%')
                ->orWhere('description', 'LIKE', '%' . Req::get('search') . '%')
                ->latest()
                ->paginate(10);
            return view('backend.companies.index')->with('companies', $result);
        }
        return view('backend.companies.index')->with('companies', Company::latest()->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'image' => 'image',
        ]);
        $image = $request->default_image;

        $image_new_name = time().$image->getClientOriginalName();
        $image->move('img/companies', $image_new_name);

        $company = Company::create([
            'name'=> $request->name,
            'description'=> $request->description,
            'slug'=> str_slug($request->name),
            'image'=> 'img/companies/'.$image_new_name
        ]);
        if (count($request->en)) {
            $translation = $company->translateOrNew('en');
            $translation->name = $request->en['name'];
            $translation->description = $request->en['description'];
            $translation->company_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['name']);
            $translation->save();
        }
        Session::flash('success', trans('messages.company_create_successed'));

        return redirect()->route('companies.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('backend.companies.edit')
            ->with('translation', $company->translateOrNew('en', true))
            ->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'default_image' => 'image',
        ]);

        $company = Company::find($id);

        if($request->hasFile('default_image'))
        {
            $image = $request->default_image;
            $image_new_name = time().$image->getClientOriginalName();
            $image->move('img/companies', $image_new_name);
            $company->default_image = 'img/companies/'.$image_new_name;
        }

        $company->name = $request->name;
        $company->description = $request->description;
        $company->slug = str_slug($request->name);

        if (count($request->en)) {
            $translation = $company->translateOrNew('en');
            $translation->name = $request->en['name'];
            $translation->description = $request->en['description'];
            $translation->company_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['name']);
            $translation->save();
        }

        $company->save();

        Session::flash('success', trans('messages.company_update_successed'));

        return redirect()->route('companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();


        Session::flash('success', trans('messages.company_kill_successed'));

        return redirect()->route('companies.index');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function kill($id)
    {
        $company = Company::withTrashed()->where('id', $id)->first();
        $company->forceDelete();


        Session::flash('success', trans('messages.company_kill_successed'));

        return redirect()->route('companiestrashed');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $company = Company::withTrashed()->where('id', $id)->first();
        $company->restore();


        Session::flash('success', trans('messages.company_restore_successed'));

        return redirect()->route('companiestrashed');
    }

    /**
     * Manage trashed resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {
        $companies = Company::onlyTrashed()->get();

        return view('backend.companies.trashed')->with('companies', $companies);
    }
}
