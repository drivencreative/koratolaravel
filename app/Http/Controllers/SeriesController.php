<?php

namespace App\Http\Controllers;

use App\Company;
use App\Product;
use App\Series;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Request as Req;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Sets the parameters from the get request to the variables.
        if (Req::get('search')) {

            // Perform the query using Query Builder
            $result = DB::table('series')
                ->select(DB::raw("*"))
                ->where('name', 'LIKE', '%' . Req::get('search') . '%')
                ->orWhere('description', 'LIKE', '%' . Req::get('search') . '%')
                ->latest()
                ->paginate(10);
            return view('backend.series.index')->with('series', $result);
        }
        return view('backend.series.index')->with('series', Series::latest()->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.series.create')->with('companies', Company::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:255',
            'image' => 'image',
        ]);
        $image = $request->image;

        $image_new_name = time().$image->getClientOriginalName();
        $image->move('img/series', $image_new_name);

        $series = Series::create([
            'name'=> $request->name,
            'description'=> $request->body,
            'in_menu'=> $request->in_menu,
            'image'=> 'img/series/'.$image_new_name
        ]);

        if (count($request->en)) {
            $translation = $series->translateOrNew('en');
            $translation->name = $request->en['name'];
            $translation->description = $request->en['description'];
            $translation->series_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['name']);
            $translation->save();
        }

        Session::flash('success', trans('messages.series_create_successed'));

        return redirect()->route('series.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $series = Series::localizeModel($slug);
        if (!$series) {
            return \App::abort(404);
        }
        $randomProducts = Product::all()->random(3);
        return view('series', [
            'series' => $series,
            'title' => $series->name,
            'news' => 0,
            'randomProducts' => $randomProducts
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $series = Series::find($id);
        return view('backend.series.edit')
            ->with('series', $series)
            ->with('translation', $series->translateOrNew('en', true))
            ->with('companies', Company::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'image' => 'image',
        ]);
        $series = Series::find($id);

        if($request->hasFile('image'))
        {
            $image = $request->image;
            $image_new_name = time().$image->getClientOriginalName();
            $image->move('img/series', $image_new_name);
            $series->image = 'img/series/'.$image_new_name;
        }

        $series->name = $request->name;
        $series->description = $request->description;
        $series->in_menu  = $request->has('in_menu') ? 1 : 0;

        if (count($request->en)) {
            $translation = $series->translateOrNew('en');
            $translation->name = $request->en['name'];
            $translation->description = $request->en['description'];
            $translation->series_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['name']);
            $translation->save();
        }
        $series->save();

        Session::flash('success', trans('messages.series_update_successed'));

        return redirect()->route('series.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $series = Series::find($id);
        $series->delete();


        Session::flash('success', trans('messages.series_delete_successed'));

        return redirect()->route('series.index');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function kill($id)
    {
        $series = Series::withTrashed()->where('id', $id)->first();
        $series->forceDelete();


        Session::flash('success', trans('messages.series_kill_successed'));

        return redirect()->route('series.trashed');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $series = Series::withTrashed()->where('id', $id)->first();
        $series->restore();


        Session::flash('success', trans('messages.series_restore_successed'));

        return redirect()->route('series.trashed');
    }

    /**
     * Manage trashed resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {
        $series = Series::onlyTrashed()->get();

        return view('backend.series.trashed')->with('series', $series);
    }
}
