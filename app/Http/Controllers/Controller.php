<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\News;
use App\Product;
use App\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $news = News::query()->latest()->paginate(2);

        $randomProducts = Product::all()->random(3);

        return view('index', [
            'news' => $news,
            'localeCode' => app()->getLocale(),
            'randomProducts' => $randomProducts,
        ]);

    }
    public function send()
    {
        $objMailer = new \stdClass();
        $objMailer->mailer_one = 'Mailer One Value';
        $objMailer->mailer_two = 'Mailer Two Value';
        $objMailer->sender = 'Laravel App';
        $objMailer->receiver = 'Milos Radisic';

        Mail::to("scorrp@gmail.com")->send(new Mailer($objMailer));
    }
}
