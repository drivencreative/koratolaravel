<?php

namespace App\Http\Controllers;

use Session;
use App\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.settings.index')->with('settings', Settings::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file_upload && $request->hasFile('value')) {
            $file = $request->value;
            $file_new_name = date("d-m-y") . '-' . $file->getClientOriginalName();
            $file->move('files/', $file_new_name);
        }
        Settings::create([
            'key' => $request->key,
            'display_name' => $request->display_name,
            'value' => ($request->file_upload && $request->hasFile('value')) ? 'files/' . $file_new_name : $request->value,
            'details' => $request->details,
            'type' => 'text',
        ]);

        Session::flash('success', trans('messages.setting_create_successed'));

        return redirect()->route('setting.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Settings $settings
     * @return \Illuminate\Http\Response
     */
    public function show(Settings $settings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Settings::find($id);
        return view('backend.settings.edit')->with('setting', $setting)->with('settings', Settings::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('vendor.adminlte.passwords.reset', [
            'token' => auth()->user()->token,
            'email' => auth()->user()->email,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $setting = Settings::find($id);

        $setting->key = $request->key;
        $setting->display_name = $request->display_name;
        $setting->value = $request->value;
        $setting->details = $request->details;
        $setting->save();

        Session::flash('success', trans('messages.setting_update_successed'));

        return redirect()->route('setting.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting = Settings::find($id);
        $setting->delete();


        Session::flash('success', trans('messages.setting_kill_successed'));

        return redirect()->route('setting.index');
    }
}
