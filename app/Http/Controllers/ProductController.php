<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductTranslation;
use App\Series;
use Illuminate\Support\Facades\DB;
use Session;
use Request as Req;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * Product List View
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        return view('index', ['products' => Product::paginate(2)]);
    }

    /**
     * Product Detail View
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $product = Product::localizeModel($slug);
        if (!$product) {
            return \App::abort(404);
        }
        return view(
            'product',
            [
                'title' => $product->name,
                'product' => $product,
                'randomProducts' => Product::all()->random(3),
                'products' => Product::all()->random(3),
                'news' => 0
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Sets the parameters from the get request to the variables.
        if (Req::get('search')) {

            // Perform the query using Query Builder
            $result = DB::table('products')
                ->select(DB::raw("*"))
                ->where('name', 'LIKE', '%' . Req::get('search') . '%')
                ->orWhere('description', 'LIKE', '%' . Req::get('search') . '%')
                ->latest()
                ->paginate(10);
            return view('backend.products.index')->with('products', $result);
        }
        return view('backend.products.index')->with('products', Product::latest()->paginate(10));
    }

    /**
     * find Product by slug
     *
     * @param $slug
     * @return $this
     */
    public function getProduct($slug)
    {

        $product = Product::where('slug', $slug);
        $product = $product->firstOrFail();
        $randomProducts = Product::all()->random(3);
        return view('product',
            [
                'product' => Product::where('slug', $slug)->first(),
                'randomProducts' => $randomProducts,
                'news' => 0
            ]
        )->with('product', $product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.products.create')->with('series', Series::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'image' => 'image',
        ]);
        $image = $request->image;

        $image_new_name = time() . $image->getClientOriginalName();
        $image->move('img/products', $image_new_name);

        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'is_new' => isset($request->is_new),
            'slug' => str_slug($request->title),
            'image' => 'img/products/' . $image_new_name,
            'specification' => $request->specification,
            'price_eu' => $request->price_eu,
            'price_rsd' => $request->price_rsd,
            'price_description' => $request->price_description
        ]);

        if (count($request->en)) {
            $translation = $product->translateOrNew('en');
            $translation->name = $request->en['name'];
            $translation->description = $request->en['description'];
            $translation->specification = $request->en['specification'];
            $translation->price_description = $request->en['price_description'];
            $translation->product_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['name']);
            $translation->save();
        }

        Session::flash('success', trans('messages.product_create_successed'));

        return redirect()->route('product.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backend.products.edit')
            ->with('product', Product::find($id))
            ->with('translation', Product::find($id)->translateOrNew('en', true))
            ->with('series', Series::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'image' => 'image',
        ]);

        $product = Product::find($id);

        if ($request->hasFile('image')) {
            $image = $request->image;
            $image_new_name = time() . $image->getClientOriginalName();
            $image->move('img/products', $image_new_name);
            $product->image = 'img/products/' . $image_new_name;
        }

        $product->name = $request->name;
        $product->description = $request->description;
        $product->slug = str_slug($request->name);
        $product->specification = $request->specification;
        $product->is_new = isset($request->is_new);
        $product->price_eu = $request->price_eu;
        $product->price_rsd = $request->price_rsd;
        $product->price_description = $request->price_description;
        $product->series_id = $request->series_id;

        if (count($request->en)) {
            $translation = $product->translateOrNew('en');
            $translation->name = $request->en['name'];
            $translation->description = $request->en['description'];
            $translation->specification = $request->en['specification'];
            $translation->price_description = $request->en['price_description'];
            $translation->product_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['name']);
            $translation->save();
        }

        $product->save();

        Session::flash('success', trans('messages.product_update_successed'));

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();


        Session::flash('success', trans('messages.product_kill_successed'));

        return redirect()->route('product.index');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function kill($id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();
        $product->forceDelete();


        Session::flash('success', trans('messages.product_kill_successed'));

        return redirect()->route('product.trashed');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();
        $product->restore();


        Session::flash('success', trans('messages.product_restore_successed'));

        return redirect()->route('product.trashed');
    }

    /**
     * Manage trashed resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {
        $products = Product::onlyTrashed()->get();

        return view('backend.products.trashed')->with('products', $products);
    }
}
