<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Session;
use Request as Req;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Sets the parameters from the get request to the variables.
        if (Req::get('search')) {
            $table = (\App::getLocale() == config('locale')) ? 'news' : 'news_translations';
            // Perform the query using Query Builder
            $result = DB::table($table)
                ->select(DB::raw("*"))
                ->where('title', 'LIKE', '%' . Req::get('search') . '%')
                ->orWhere('text', 'LIKE', '%' . Req::get('search') . '%')
                ->latest()
                ->paginate(10);
            return view('backend.news.index')->with('news', $result);
        }
        $news = News::latest()->paginate(10);
        return view('backend.news.index')->with('news', $news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.news.create');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
        ];
        if ($request->image) {
            $photos = count($request->image);
            foreach (range(0, $photos) as $index) {
                $rules['image.' . $index] = 'image';
            }
        }

        return $rules;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            $this->rules($request)
        ]);
        $images = '';
        if ($request->image) {
            foreach ($request->image as $image) {
                $image_new_name = time() . $image->getClientOriginalName();
                $image->move('img/news', $image_new_name);
                if (!empty($images)) $images .= '|';
                $images .= $image_new_name ? 'img/news/' . $image_new_name : '';
            }
        }

        $news = News::create([
            'title' => $request->title,
            'text' => $request->text,
            'slug' => str_slug($request->title),
            'image' => $images
        ]);

        if (count($request->en)) {
            $translation = $news->translateOrNew('en');
            $translation->title = $request->en['title'];
            $translation->text = $request->en['text'];
            $translation->news_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['title']);
            $translation->save();
        }
        Session::flash('success', trans('messages.news_create_successed'));

        return redirect()->route('news.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('index', ['news' => News::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('backend.news.edit')
            ->with('news', News::find($id))
            ->with('translation', $news->translateOrNew('en', true))
            ->with('images', explode("|", $news->image));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param News $news
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, News $news)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'image' => 'image',
        ]);


        if ($request->hasFile('image')) {
            $image = $request->image;
            $image_new_name = time() . $image->getClientOriginalName();
            $image->move('img/news', $image_new_name);
            $news->image = 'img/news/' . $image_new_name;
        }

        $news->title = $request->title;
        $news->text = $request->text;
        $news->slug = str_slug($request->title);

        if (count($request->en)) {
            $translation = $news->translateOrNew('en');
            $translation->title = $request->en['title'];
            $translation->text = $request->en['text'];
            $translation->news_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['title']);
            $translation->save();
        }

        $news->save();

        Session::flash('success', trans('messages.news_update_successed'));

        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        if ($news) {
            $news->delete();
        }


        Session::flash('success', trans('messages.news_delete_successed'));

        return redirect()->route('news.index');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function kill($id)
    {
        $news = News::withTrashed()->where('id', $id)->first();
        if ($news) {
            $news->forceDelete();
        }


        Session::flash('success', trans('messages.news_kill_successed'));

        return redirect()->route('news.trashed');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $news = News::withTrashed()->where('id', $id)->first();
        $news->restore();


        Session::flash('success', trans('messages.news_restore_successed'));

        return redirect()->route('news.trashed');
    }

    /**
     * Manage trashed resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {
        $news = News::onlyTrashed()->get();

        return view('backend.news.trashed')->with('news', $news);
    }
}
