<?php

namespace App\Http\Controllers;

use Session;
use App\Product;
use Illuminate\Http\Request;
use App\Page;
use App\PageTranslation;

class PageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.index')->with('pages', Page::paginate(10));
    }

    /**
     * Display a dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('backend.dashboard');
    }

    /**
     * find Page by slug
     *
     * @param $slug
     * @return $this
     */
    public function show($slug)
    {

        $page = Page::getBySlug($slug);
        if (!$page) {
            return \App::abort(404);
        }
        $randomProducts = Product::all()->random(3);
        return view('page',
            [
                'title' => $page->title,
                'randomProducts' => $randomProducts,
                'news' => 0
            ]
        )->with('page', $page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.create')->with('pages', Page::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'image' => 'image',
        ]);
        $image = $request->image;
        $image_new_name = '';
        if ($request->hasFile('image')) {
            $image_new_name = time() . $image->getClientOriginalName();
            $image->move('img/pages', $image_new_name);
        }
        $page = Page::create([
//            'code' => 'sr',
            'title' => $request->title,
            'body' => $request->body,
            'page_id' => $request->page_id,
            'status' => $request->status ?? 0,
            'slug' => str_slug($request->title),
            'image' => $image_new_name ? 'img/pages/' . $image_new_name : '',

        ]);
        $page->translateOrNew('en')->title = $request->en['title'];
        $page->translateOrNew('en')->body = $request->en['body'];
        $page->translateOrNew('en')->meta_description = $request->en['meta_description'];
        $page->translateOrNew('en')->meta_keywords = $request->en['meta_keywords'];
        $page->translateOrNew('en')->slug = str_slug($request->en['title']);

        if($page->save()) Session::flash('success', trans('messages.page_create_successed'));

        return redirect()->route('page.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);
        return view('backend.pages.edit')
            ->with('page', $page)
            ->with('pages', Page::all())
            ->with('translation', $page->translateOrNew('en', true));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'image' => 'file',
        ]);

        $page = Page::find($id);

        if ($request->hasFile('image')) {
            $image = $request->image;
            $image_new_name = time() . $image->getClientOriginalName();
            $image->move('img/pages', $image_new_name);
            $page->image = 'img/pages/' . $image_new_name;
        }

        $page->title = $request->title;
        $page->body = $request->body;
        $page->page_id = $request->page_id ?: null;
        $page->slug = str_slug($request->title);
        $page->status = $request->status ?: 0;


        if (count($request->en)) {
            $translation = $page->translateOrNew('en');
            $translation->title = $request->en['title'];
            $translation->body = $request->en['body'];
            $translation->meta_description = $request->en['meta_description'];
            $translation->meta_keywords = $request->en['meta_keywords'];
            $translation->page_id = $request->id ?: null;
            $translation->slug = str_slug($request->en['title']);
            $translation->save();
        }

        $page->save();
        Session::flash('success', trans('messages.page_update_successed'));

        return redirect()->route('page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();

        Session::flash('success', trans('messages.page_destroy_successed'));

        return redirect()->route('page.index');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function kill($id)
    {
        $page = Page::withTrashed()->where('id', $id)->first();
        $page->forceDelete();


        Session::flash('success', trans('messages.page_kill_successed'));

        return redirect()->route('page.trashed');
    }

    /**
     * Remove the trashed specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $page = Page::withTrashed()->where('id', $id)->first();
        $page->restore();


        Session::flash('success', trans('messages.page_restore_successed'));

        return redirect()->route('page.trashed');
    }

    /**
     * Manage trashed resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed()
    {
        $pages = Page::onlyTrashed()->get();

        return view('backend.pages.trashed')->with('pages', $pages);
    }
}
