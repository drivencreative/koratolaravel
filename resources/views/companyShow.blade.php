@extends('index')

@section('content')
    <div class="right_holder">

        <div class="arial_11_company" style="padding-left:20px">
            <h1>{{$company->name}}</h1>
        </div>
        <div class="spacer_right">
        </div>
        <div class="description">
            <div class="arial_11_4d">
                @lang('messages.description')
                <br/>
                <br/>
                <br/>
            </div>
            @if($company->series)
                <div class="arial_11_8b">
                    @foreach($company->series as $item)
                        @include('partials.series', ['series' => $item])
                    @endforeach
                </div>
            @endif
        </div>
        <br>
        <br>
        <div class="new_products_big">
            <div class="arial_11_4d" style="padding-left:15px">
                <br><br>
            </div>
        </div>
    </div>
@stop