<nav class="nav" id="korato-nav">
    <ul class="nav__menu">
        @includeIf('partials.navigation.companies', ['companies' => \App\Company::all()])
        @includeIf('partials.navigation.series', ['series' => \App\Series::where('in_menu','=', 1)->get()])
        @includeIf('partials.navigation.pages', ['pages' => \App\Page::all()])
        @includeIf('partials.navigation.language')
    </ul>
</nav>