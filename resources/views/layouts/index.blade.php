<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}"{!! (config('lang.direction')=='rtl') ? ' dir="rtl"' : '' !!}>
<head>
{{--    {{ dd(get_defined_vars()) }}--}}
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Korato pojačala i predpojačala, HiFi sistemi, zvučnickih jedinica, kitovi za samograditelje, konektori, kablovi. Najviši kvalitet po prihvatljivim cenama.">
    <meta name="author" content="Korato">
    <meta http-equiv="keywords"
          content="{{ $keywords }}">
    <title>{{ $title }}</title>

<!-- Custom styles for this template -->
    <!-- app -->
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <!-- Lightbox style -->
    <link rel="stylesheet" href="{{ URL::asset('js/lightbox2/dist/css/lightbox.min.css') }}">

</head>

<body>
<div class="main_white_page">
    <div class="header">
        <div class="header_left_logo">
            <a href="/">
                <img src="/img/korato_logo_184_86.png" alt="Korato Group - Have a good sound around" width="184"
                     height="86"/>
            </a>
        </div>
        <div class="header_right_menu" style="position: relative;">
            <div class="top-contact-info">
                <a href="/kontakt" class="header_link_address">
                    Serbia, Belgrade, Mileševska 66
                    <br/>
                    +381 11 3047-189<br/>
                    +381 11 3047-199
                </a>
            </div>
            <div class="main-navigation">
                <!-- Navigation -->
                @include('menu')
            </div>
        </div>
    </div>
    <div class="baner_big">
        <div class="banner_left">
            <script src="{{'/js/swfobject_modified.min.js'}}" type="text/javascript"></script>
            <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="590" height="262" style="visibility: visible;">
                <param name="movie" value="{{ asset('/baner/baner.swf') }}">
                <param name="quality" value="high">
                <param name="wmode" value="opaque">
                <param name="swfversion" value="6.0.65.0">
                <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
                <param name="expressinstall" value="{{'/Scripts/expressInstall.swf'}}">
                <param name="flashvars" value="direktorijum={{'/baner/korato/lb_korato.jpg','/baner/korato/10.jpg','/baner/korato/2.jpg','/baner/korato/7.jpg','/baner/korato/3.jpg','/baner/korato/6.jpg','/baner/korato/9.jpg','/baner/korato/8.jpg','/baner/korato/4.jpg','/baner/korato/5.jpg'}}">
                <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="{{ asset('/baner/baner.swf') }}" width="590" height="262">
                    <!--<![endif]-->
                    <param name="quality" value="high">
                    <param name="wmode" value="opaque">
                    <param name="swfversion" value="6.0.65.0">
                    <param name="expressinstall" value="{{'/Scripts/expressInstall.swf'}}">
                    <param name="flashvars" value="direktorijum=/baner/korato/lb_korato.jpg,/baner/korato/10.jpg,/baner/korato/2.jpg,/baner/korato/7.jpg,/baner/korato/3.jpg,/baner/korato/6.jpg,/baner/korato/9.jpg,/baner/korato/8.jpg,/baner/korato/4.jpg,/baner/korato/5.jpg">
                    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                    <div>
                        <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                        <p>
                            <a href="http://www.adobe.com/go/getflashplayer">
                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
                                     alt="Get Adobe Flash player" width="112" height="33">
                            </a>
                        </p>
                    </div>
                    <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
            <script type="text/javascript">
				<!--
				swfobject.registerObject("FlashID");
				//-->
            </script>
        </div>
        <div class="banner_right">
            <table width="170" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody><tr>
                    <td align="left" valign="bottom" class="arial_28_bold_white">
                        <img src="{{ asset( 'storage/' . $topRightImage )}}" border="0" alt="img" />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Left Sidebar -->
@yield('left')
<!-- Main Content -->

@yield('content')
<!-- Footer -->
    @yield('footer')
</div>

{{--<!-- jQuery 3 -->--}}
{{--<script src="{{ asset('AdminLTE/bower_components/jquery/dist/jquery.min.js') }}"></script>--}}
{{--<!-- Lightbox -->--}}
{{--<script src="{{ asset('js/lightbox2/dist/js/lightbox.min.js') }}"></script>--}}
</body>

</html>
