@extends('index')
@section('left')
    <div class="left_holder">
        <div class="arial_11_orange"></div>
        <div class="spacer_left">
        </div>
        <div class="arial_11_8b">
            @if(!empty($product->series))
                @if($product->series->image && file_exists( public_path($product->series->image)))
                    <img src="{{ asset($product->series->image) }}" width="130px">
                @endif
                    <div style="padding-left:20px">
                        {{$product->name}}
                        <br>
                        <img src="{{ asset('img/gradient_new_products.jpg') }}" alt="grad" width="130" height="3"
                             border="0">
                    </div>
            @endif
        </div>
    </div>
@stop
@section('content')
    <div class="right_holder">
        @if(!empty($product->series))
            <span class="arial_11_company" style="padding-left:20px">
            <b>
                <a href="{{ route('series', ['slug'=> $product->series->company->slug] ) }}"
                   class="link_arial_11_company">
                    {{ $product->series->company->name }}
                </a>
            </b>
            <b>
                <span class="arial_11_8b"> &gt; </span>
                <a href="{{ route('series', ['slug'=> $product->series->slug] ) }}" class="link_arial_11_darkgray">
                    {{$product->series->name}}
                </a>
            </b>
        </span>
        @endif
        <b>
            <span class="arial_11_8b"> &gt; </span>
            <span class="arial_11_8b"> @if(!empty($product->name)){{$product->name}}@endif</span>
        </b>
        <div class="spacer_right">
        </div>
        <div class="description">
            <div class="arial_11_4d">
                <span class="arial_11_8b">
                    @if(!empty($product->name))
                        <h1>{{$product->name}}</h1>
                    @endif
                </span>
                <b>{{ trans('messages.description') }}</b>
                <br><br>
            </div>
            <div class="arial_11_8b">
                @if($product->image && file_exists( public_path($product->image)))
                    <img src="{{ asset($product->image) }}" width="350px">
                @endif
                <br><br>
            </div>
            <div class="arial_11_8b">
                @if(!empty($product->description))
                    {!! $product->description !!}
                @endif
                <br><br>
            </div>

        </div>
        <br>
        <br>
        <div class="new_products_big">
            <div class="arial_11_4d" style="padding-left:15px">
                @if(!empty($product->specification))
                {!! $product->specification !!}
                @endif
            </div>
        </div>
    </div>
@stop