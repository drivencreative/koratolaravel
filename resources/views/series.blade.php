@extends('layouts.index')
@section('left')
    @include('partials.leftColumn', ['randomProducts' => $randomProducts])
@stop
@section('content')
    <div class="right_holder">
        {{--{{ dd(get_defined_vars()) }}--}}
        <div class="arial_11_company" style="padding-left:20px">
            <h1>{{ $series->name }}</h1>
        </div>
        <div class="spacer_right">
        </div>
        <div class="description">
            <div class="arial_11_4d"></div>

            <div class="arial_11_8b">

            </div>
        </div>
        <br>
        <br>
        <div class="new_products_big">
            <div class="arial_11_4d" style="padding-left:15px">
                <br><br>
            </div>
            @foreach($series->products as $product)
                <div class="series_page series_images">
                    <a href="{{ route('product', ['slug'=> $product->slug] ) }}" class="link_arial_11_darkgray">
                        @if(file_exists( public_path($product->image)))
                            <img src="{{ asset($product->image) }}" width="130px">
                        @endif
                        <br>&nbsp;&nbsp;&nbsp;
                        <b>{{$product->name}}</b>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@stop
@section('footer')
    @include('partials.footer')
@stop