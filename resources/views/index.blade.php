@extends('layouts.index')

@section('left')
    @include('partials.leftColumn', ['randomProducts' => $randomProducts])
@stop
@section('content')
    <div class="right_holder">

        <div class="arial_11_company" style="padding-left:20px">
            <b>{{ trans('messages.news') }}</b>
        </div>
        <div class="spacer_right">
        </div>
        <div class="description">
            <div class="arial_11_4d"></div>
            @if($news)

                <div class="arial_11_8b">
                    @foreach($news as $item)
                            @include('partials.news', ['news' => $item])
                    @endforeach

                    {{ $news->links() }}
                </div>
            @endif
        </div>
        <br>
        <br>
        <div class="new_products_big">
            <div class="arial_11_4d" style="padding-left:15px">
                <br><br>
            </div>
        </div>
    </div>
@stop
@section('footer')
    @include('partials.footer')
@stop