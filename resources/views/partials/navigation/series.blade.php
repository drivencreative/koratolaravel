<li class="nav__menu-item">
    <a href="#">
        <span>{{ trans('messages.menu.series') }}</span>
    </a>
    <ul class="nav__submenu">
        @foreach ($series as $seriesItems)
            <li class="nav__submenu-item">
                <a href="{{ route('series', ['slug'=> $seriesItems->slug] ) }}">
                    <span>{{ $seriesItems->name }}</span>
                </a>
            </li>
        @endforeach
    </ul>
</li>