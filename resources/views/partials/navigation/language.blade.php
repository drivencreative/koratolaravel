@foreach( LaravelLocalization::getSupportedLocales() as $localeCode => $properties )
    @php($defaultLocale = app('laravellocalization')->getDefaultLocale())
    <li class="nav__menu-item">
        <a rel="alternate" hreflang="{{ $localeCode }}"
           href="/@if($defaultLocale != $localeCode){{$localeCode}} @endif">
            {{ $properties['native'] }}
        </a>
    </li>
@endforeach
