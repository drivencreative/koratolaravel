<li class="nav__menu-item">
    @if($pages->count())
        @foreach ($pages as $page)
            @if($page->child->count())
                <a href="#" class="nav-link">
                    <span>{{ trans('messages.menu.korato') }}</span>
                </a>
                <ul class="nav__submenu">
                    @foreach ($page->child as $childPage)
                        <li class="nav__submenu-item">
                            <a href="{{ route('page', ['slug'=> $childPage->slug ]) }}"
                               class="nav-link">
                                <span>{{ $childPage->title }}</span>
                            </a>
                        </li>
                    @endforeach
                    @if(\App\Settings::where('key', 'site.pricelist')->first()['value'])
                        <li class="nav__submenu-item">
                            <a href="/{{ \App\Settings::where('key', 'site.pricelist')->first()['value']}}"
                               class="nav-link">
                                <span>{{ \App\Settings::where('key', 'site.pricelist')->first()['display_name']}}</span>
                            </a>
                        </li>
                    @endif
                </ul>
            @endif
        @endforeach
    @endif
</li>