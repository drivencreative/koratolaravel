<li class="nav__menu-item">
    <a href="#">
        <span>{{ trans('messages.menu.companies') }}</span>
    </a>
    <ul class="nav__submenu">
        @foreach ($companies as $company)
            <li class="nav__submenu-item">
                <a href="{{ route('company', ['slug'=> $company->slug] ) }}">
                    <span>{{ $company->name }}</span>
                </a>
            </li>
        @endforeach
    </ul>
</li>