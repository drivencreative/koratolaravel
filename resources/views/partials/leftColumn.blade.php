<div class="left_holder">
    <div class="arial_11_orange"><b>{{trans('messages.newProducts')}}</b></div>
    <div class="spacer_left">
        @foreach($randomProducts as $product)
            @include('partials.productRandom', ['product' => $product])
        @endforeach
    </div>
</div>