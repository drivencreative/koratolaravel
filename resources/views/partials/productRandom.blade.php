<div class="randomNewProduct series_images" style="text-align: center; padding-top: 2em;">
    <a href="{{ route('product', ['slug'=> $product->slug] ) }}" class="link_arial_11_darkgray">
        @if(file_exists( public_path($product->image)))
            <img src="{{ asset($product->image) }}" width="130px">
        @endif
        <br>&nbsp;&nbsp;&nbsp;
        <b>{{$product->name}}</b>
    </a>
</div>

