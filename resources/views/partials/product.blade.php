<div class="post-preview">
    <h2 class="post-title">
        <a href="#">
            {{$product->name}}
        </a>
    </h2>
    <h3 class="post-subtitle">
        {!! $product->description !!}
    </h3>
    <p class="post-meta">Posted by
        <a href="#">Start Bootstrap</a>
        on {{ date('F d, Y', strtotime($product->created_at)) }}</p>
</div>
<hr>