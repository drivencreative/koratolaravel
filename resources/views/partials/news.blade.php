<div class="news-item">
    @if($news->translate(app()->getLocale())->title)
        <h2 class="news-title">
            {{$news->translate(app()->getLocale())->title}}
        </h2>
    @endif
    @if($news->image)
        <div class="news-image">
            @if(file_exists( public_path($news->image)))
                <img src="{{ asset($news->image) }}" height="90px">
            @endif
        </div>
    @endif
    @if($news->translate(app()->getLocale())->text)
        <div class="news-text">
            {!! $news->translate(app()->getLocale())->text !!}
        </div>
    @endif
    <p class="news-meta">{{ trans('messages.published') }} {{ date('F d, Y', strtotime($news->created_at)) }}</p>
</div>
<hr>