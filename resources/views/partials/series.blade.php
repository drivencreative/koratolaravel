<div class="company_page series_images">
    <a href="{{ route('series', ['slug'=> $series->slug] ) }}" class="link_arial_11_darkgray">
        @if(file_exists( public_path($series->image)))
            <img src="{{ asset($series->image) }}" width="130px">
        @endif
        <br>
        {{$series->title}}
    </a>
</div>