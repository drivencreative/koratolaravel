
@extends('layouts.index')

@section('left')
    @include('partials.leftColumn', ['randomProducts' => $randomProducts])
@stop
@section('content')
    <div class="right_holder">
        @if($page)
            <div class="arial_11_company" style="padding-left:20px">
                <b>{{ $page->title }}</b>
            </div>
        @endif
        <div class="spacer_right">
        </div>
        <div class="description">
            <div class="arial_11_4d"></div>
            @if($page)
                <div class="arial_11_8b">
                    {!! $page->body  !!}
                </div>
            @endif
        </div>
        <br>
        <br>
        <div class="new_products_big">
            <div class="arial_11_4d" style="padding-left:15px">
                <br><br>
            </div>
        </div>
    </div>
@stop
@section('footer')
    @include('partials.footer')
@stop