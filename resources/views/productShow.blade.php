@extends('index')
@section('left')
    <div class="left_holder">
        <div class="arial_11_orange"></div>
        <div class="spacer_left">
            @if(file_exists( public_path($series->image)))
                <img src="{{ asset($series->image) }}" width="130px">
            @endif
        </div>
    </div>
@stop
@section('content')
    <div class="right_holder">
        <div class="arial_11_company" style="padding-left:20px">
            <b>{{$product->name}}</b>
        </div>
        <div class="spacer_right">
        </div>
        <div class="description">
            <div class="arial_11_4d">
                {!! $product->description !!}
            </div>
            <div class="arial_11_8b">
                {!! $product->specification !!}
            </div>
        </div>
        <br>
        <br>
        <div class="new_products_big">
            <div class="arial_11_4d" style="padding-left:15px">
                <br><br>
            </div>
        </div>
    </div>
@stop