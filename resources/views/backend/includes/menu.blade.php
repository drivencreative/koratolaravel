<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="active treeview">
        <a href="#">
            <i class="fa fa-list"></i>
            <span>{{ trans('messages.pages') }}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right">
                </i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li
                @if(route('page.index') == Request::url())
                class="active"
                @endif
            >
                <a href="{{ route('page.index') }}">
                    <i class="fa fa-files-o"></i> {{ trans('messages.list') }}
                </a>
            </li>
            <li
                @if(route('page.create') == Request::url())
                class="active"
                @endif
            >
                <a href="{{ route('page.create') }}">
                    <i class="fa fa-file-o"></i> {{ trans('messages.new_f') }}
                </a>
            </li>
            <li
                @if(route('page.trashed') == Request::url())
                class="active"
                @endif
            >
                <a href="{{ route('page.trashed') }}">
                    <i class="fa fa-trash-o"></i> {{ trans('messages.deleted_f') }}
                </a>
            </li>
        </ul>
    </li>
    <li class="active treeview">
        <a href="#">
            <i class="fa fa-product-hunt"></i>
            <span>{{ trans('messages.products') }}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right">
                </i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li
                @if(route('product.index') == Request::url())
                class="active"
                @endif
            >
                <a href="{{ route('product.index') }}">
                    <i class="fa fa-files-o"></i> {{ trans('messages.list') }}
                </a>
            </li>
            <li
                @if(route('product.create') == Request::url())
                class="active"
                @endif
            >
                <a href="{{ route('product.create') }}">
                    <i class="fa fa-file-o"></i> {{ trans('messages.new_m') }}
                </a>
            </li>
            <li
                @if(route('product.trashed') == Request::url())
                class="active"
                @endif
            >
                <a href="{{ route('product.trashed') }}">
                    <i class="fa fa-trash-o"></i> {{ trans('messages.deleted_m') }}
                </a>
            </li>
        </ul>
    </li>
    <li class="active treeview">
        <a href="#">
            <i class="fa fa-building"></i>
            <span>{{ trans('messages.companies') }}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right">
                </i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li
                    @if(route('companies.index') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('companies.index') }}">
                    <i class="fa fa-files-o"></i> {{ trans('messages.list') }}
                </a>
            </li>
            <li
                    @if(route('companiescreate') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('companiescreate') }}">
                    <i class="fa fa-file-o"></i> {{ trans('messages.new_m') }}
                </a>
            </li>
            <li
                    @if(route('companiestrashed') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('companiestrashed') }}">
                    <i class="fa fa-trash-o"></i> {{ trans('messages.deleted_f') }}
                </a>
            </li>
        </ul>
    </li>
    <li class="active treeview">
        <a href="#">
            <i class="fa fa-folder-open"></i>
            <span>{{ trans('messages.series') }}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right">
                </i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li
                    @if(route('series.index') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('series.index') }}">
                    <i class="fa fa-files-o"></i> {{ trans('messages.list') }}
                </a>
            </li>
            <li
                    @if(route('series.create') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('series.create') }}">
                    <i class="fa fa-file-o"></i> {{ trans('messages.new_m') }}
                </a>
            </li>
            <li
                    @if(route('series.trashed') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('series.trashed') }}">
                    <i class="fa fa-trash-o"></i> {{ trans('messages.deleted_f') }}
                </a>
            </li>
        </ul>
    </li>
    <li class="active treeview">
        <a href="#">
            <i class="fa fa-folder-open"></i>
            <span>{{ trans('messages.news') }}</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right">
                </i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li
                    @if(route('news.index') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('news.index') }}">
                    <i class="fa fa-files-o"></i> {{ trans('messages.list') }}
                </a>
            </li>
            <li
                    @if(route('news.create') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('news.create') }}">
                    <i class="fa fa-file-o"></i> {{ trans('messages.new_m') }}
                </a>
            </li>
            <li
                    @if(route('news.trashed') == Request::url())
                    class="active"
                    @endif
            >
                <a href="{{ route('news.trashed') }}">
                    <i class="fa fa-trash-o"></i> {{ trans('messages.deleted_f') }}
                </a>
            </li>
        </ul>
    </li>
    <li class="
    @if(route('setting.index') == Request::url())
        active
    @endif
     ">
        <a href="{{ route('setting.index') }}">
            <i class="fa fa-cogs"></i>
            <span>{{ trans('messages.settings') }}</span>
        </a>
    </li>
</ul>
<!-- /.sidebar -->