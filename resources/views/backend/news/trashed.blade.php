@extends('adminlte::page')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.news') }}</h2>
        </div>
        <div class="box-body">
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{ trans('messages.image') }}</th>
                    <th>{{ trans('messages.title') }}</th>
                    <th>{{ trans('messages.text') }}</th>
                    <th>{{ trans('messages.to_edit') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($news->count())
                    @foreach($news as $newsItem)
                        <tr>
                            <td>
                                @if(file_exists( public_path($newsItem->image)))
                                    <img src="{{ asset($newsItem->image) }}" height="30px">
                                @else
                                    <i class="fa fa-file-image-o fa-lg"></i>
                                @endif
                            </td>
                            <td>
                                {{ $newsItem->title }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($newsItem->text), 30) }}
                            </td>
                            <td>
                                <a href="{{ route('news.restore', ['id'=> $newsItem->id] ) }}" class="btn btn-undo">
                                    <i class="fa fa-undo"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('news.kill', ['id'=> $newsItem->id] ) }}" class="btn btn-remove">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_trashed_news') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop