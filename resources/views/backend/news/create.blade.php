@extends('adminlte::page')

@section('content')
    @if(count($errors) > 0)
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.create_news') }}
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('news.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="title">{{ trans('messages.title') }}</label>
                            <input type="text" id="title" name="title" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="image">{{ trans('messages.image') }}</label>
                            <input type="file" id="image" name="image[]" class="form-control" multiple/>
                        </div>

                        <div class="form-group">
                            <label for="text">{{ trans('messages.text') }}</label>
                            <textarea name="text" id="text" cols="30" rows="20"
                                      class="form-control cke-editor"></textarea>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="translation">
                        <div class="form-group">
                            <label for="title-en">{{ trans('messages.title') }}</label>
                            <input type="text" title="en[title]" id="title-en"
                                   class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="text-en">{{ trans('messages.text') }}</label>
                            <textarea name="en[text]" id="text-en" cols="30" rows="20"
                                      class="form-control cke-editor"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ trans('messages.create') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop