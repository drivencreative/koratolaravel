@extends('adminlte::page')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.news') }}</h2>
        </div>
        <div class="box-body">
            <div class="col-sm-6">
                <form action="{{ route('news.index') }}" method="get">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" name="search"/>
                        <span class="input-group-btn">
                      <input type="submit" class="btn btn-flat" value="{{ trans('messages.search') }}"/>

                    </span>
                    </div>
                </form>
            </div>
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{ trans('messages.image') }}</th>
                    <th>{{ trans('messages.title') }}</th>
                    <th>{{ trans('messages.text') }}</th>
                    <th>{{ trans('messages.title') }}</th>
                    <th>{{ trans('messages.text') }}</th>
                    <th>{{ trans('messages.to_edit') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($news->count())
                    @foreach($news as $newsItem)
                        <tr>
                            <td>
                                @if(file_exists( public_path($newsItem->image)))
                                    <img src="{{ asset($newsItem->image) }}" height="30px">
                                @else
                                    <i class="fa fa-file-image-o fa-lg"></i>
                                @endif
                            </td>
                            <td>
                                {{ $newsItem->title }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($newsItem->text), 30) }}
                            </td>
                            <td>
                                {{ $newsItem->translate('en')->title }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($newsItem->translate('en')->text), 30) }}
                            </td>
                            <td>
                                <a href="{{ route('news.edit', ['id'=> $newsItem->id] ) }}" class="btn btn-success">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('news.delete', ['id'=> $newsItem->id] ) }}"
                                   class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_active_news') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {{ $news->links() }}
        </div>
    </div>
@stop