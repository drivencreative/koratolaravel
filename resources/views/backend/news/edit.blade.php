@extends('adminlte::page')

@section('content')

    @include('backend.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.edit_news') }}
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('news.update', ['id'=> $news->id] ) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="title">{{ trans('messages.title') }}</label>
                            <input type="text" id="title" name="title" value="{{ $news->title }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="image">{{ trans('messages.image') }}</label>
                            <input type="file" id="image" name="image" value="{{ $news->image }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="text">{{ trans('messages.text') }}</label>
                            <textarea name="text" id="body" cols="30" rows="20"
                                      class="form-control cke-editor">{{ $news->text }}</textarea>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="translation">
                            <div class="form-group">
                                <label for="en[title]">{{ trans('messages.title') }}</label>
                                <input type="text" name="en[title]" id="title-en" value="{{ $translation->title }}"
                                       class="form-control"/>
                            </div>

                            <div class="form-group">
                                <label for="en[text]">{{ trans('messages.text') }}</label>
                                <textarea name="en[text]" id="body-en" cols="30" rows="20"
                                          class="form-control cke-editor">{{ $translation->text }}</textarea>
                            </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ trans('messages.edit') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop