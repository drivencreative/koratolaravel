@extends('adminlte::page')

@section('content')
    @if(count($errors) > 0)
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>{{ trans('messages.create_company') }}</h2>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('companies.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input type="text" name="name" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="default_image">{{ trans('messages.default_image') }}</label>
                            <input type="file" name="default_image" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="description">{{ trans('messages.description') }}</label>
                            <textarea name="description" id="body" cols="30" rows="20"
                                      class="form-control"></textarea>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="translation">
                        <div class="form-group">
                            <label for="name-en">{{ trans('messages.name') }}</label>
                            <input type="text" name="en[name]" id="name-en"
                                   class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="description-en">{{ trans('messages.description') }}</label>
                            <textarea name="en[description]" id="body-en" cols="30" rows="20"
                                      class="form-control cke-editor"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">{{ trans('messages.create') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop