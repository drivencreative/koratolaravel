@extends('adminlte::page')

@section('content')

    @include('backend.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>{{ trans('messages.edit_company') }}</h2>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <div class="form-group">
                <h4>{{ trans('messages.belongs_to_series') }}</h4>
                <ul>
                    @foreach($company->series as $series)
                        <li>{{$series->name}}</li>
                    @endforeach
                </ul>
            </div>
            <form action="{{ route('companies.update', ['id'=> $company->id] ) }}" method="post"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input type="text" name="name" value="{{ $company->name }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="default_image">{{ trans('messages.default_image') }}</label>
                            <input type="file" name="default_image" value="{{ $company->default_image }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="description">{{ trans('messages.description') }}</label>
                            <textarea name="description" id="body" cols="30" rows="20"
                                      class="form-control cke-editor">{{ $company->description }}</textarea>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="translation">
                        <div class="form-group">
                            <label for="en[name]">{{ trans('messages.name') }}</label>
                            <input type="text" name="en[name]" id="name-en" value="{{ $translation->name }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="en[description]">{{ trans('messages.description') }}</label>
                            <textarea name="en[description]" id="body-en" cols="30" rows="20"
                                      class="form-control cke-editor">{{ $translation->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">{{ trans('messages.edit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop