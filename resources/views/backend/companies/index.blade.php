@extends('adminlte::page')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.companies') }}</h2>
        </div>
        <div class="box-body">
            <div class="col-sm-6">
                <form action="{{ route('companies.index') }}" method="get">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" name="search" />
                        <span class="input-group-btn">
                      <input type="submit" class="btn btn-flat" value="{{ trans('messages.search') }}" />

                    </span>
                    </div>
                </form>
            </div>
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>{{ trans('messages.default_image') }}</th>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.description') }}</th>
                    <th>{{ trans('messages.to_edit') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($companies->count())
                    @foreach($companies as $company)
                        <tr>
                            <td>
                                {{ $company->id }}
                            </td>
                            <td>
                                @if(file_exists( public_path($company->default_image)))
                                    <img src="{{ asset($company->default_image) }}" height="30px">
                                @else
                                    <i class="fa fa-file-image-o fa-lg"></i>
                                @endif
                            </td>
                            <td>
                                {{ $company->name }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($company->description), 30) }}
                            </td>
                            <td>
                                <a href="{{ route('companies.edit', ['id'=> $company->id] ) }}" class="btn btn-success">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('companies.delete', ['id'=> $company->id] ) }}" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_active_companies') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {{ $companies->links() }}
        </div>
    </div>
@stop