@extends('adminlte::page')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.trashed_companies') }}</h2>
        </div>
        <div class="box-body">
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>{{ trans('messages.default_image') }}</th>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.description') }}</th>
                    <th>{{ trans('messages.to_restore') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($companies->count())
                    @foreach($companies as $company)
                        <tr>
                            <td>
                                {{ $company->id }}
                            </td>
                            <td>
                                @if(file_exists( public_path($company->default_image)))
                                    <img src="{{ asset($company->default_image) }}" height="30px">
                                @else
                                    <i class="fa fa-file-image-o fa-lg"></i>
                                @endif
                            </td>
                            <td>
                                {{ $company->name }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($company->description), 30) }}
                            </td>
                            <td>
                                <a href="{{ route('companiesrestore', ['id'=> $company->id] ) }}" class="btn btn-undo">
                                    <i class="fa fa-undo"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('companieskill', ['id'=> $company->id] ) }}" class="btn btn-remove">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_trashed_pages') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop