@extends('adminlte::page')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.pages') }}</h3>
        </div>
        <div class="box-body">
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{ trans('messages.image') }}</th>
                    <th>{{ trans('messages.title') }}</th>
                    <th>{{ trans('messages.body') }}</th>
                    <th>{{ trans('messages.to_restore') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($pages->count())
                    @foreach($pages as $page)
                        <tr>
                            <td>
                                {{ $page->title }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($page->body), 30) }}
                            </td>
                            <td>
                                {{ $page->meta_description }}
                            </td>
                            <td>
                                {{ $page->meta_keywords }}
                            </td>
                            <td>
                                <a href="{{ route('page.restore', ['id'=> $page->id] ) }}" class="btn btn-undo">
                                    <i class="fa fa-undo"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('page.kill', ['id'=> $page->id] ) }}" class="btn btn-remove">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_trashed_pages') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop