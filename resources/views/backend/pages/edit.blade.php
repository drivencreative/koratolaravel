@extends('adminlte::page')

@section('content')
    @include('backend.includes.errors')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.edit_page') }}
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('page.update', ['id'=> $page->id] ) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="page_id">{{ trans('messages.parent_page') }}</label>
                            <select id="page_id" name="page_id" value="{{ $page->page_id }}" class="form-control">
                                <option value="0">{{ trans('messages.please_select') }}</option>
                                @foreach($pages as $pageItem)
                                    <option value="{{ $pageItem->id }}" @if($page->page_id == $pageItem->id) selected @endif
                                    >
                                        {{ $pageItem->title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="status" value="{{ $page->status }}"
                                           @if($page->status)
                                           checked
                                            @endif
                                    >
                                    {{ trans('messages.status') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title">{{ trans('messages.title') }}</label>
                            <input type="text" name="title" value="{{ App\Page::find($page->id)->title }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="image">{{ trans('messages.image') }}</label>
                            <input type="file" name="image" value="{{ App\Page::find($page->id)->image }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="body">{{ trans('messages.body') }}</label>
                            <textarea name="body" id="body" cols="30" rows="20"
                                      class="form-control cke-editor">{{ App\Page::find($page->id)->body }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_description">{{ trans('messages.meta_description') }}</label>
                            <textarea name="meta_description" id="meta_description" cols="30" rows="20"
                                      class="form-control">{{ App\Page::find($page->id)->meta_description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_keywords">{{ trans('messages.meta_keywords') }}</label>
                            <textarea name="meta_keywords" id="meta_keywords" cols="30" rows="10"
                                      class="form-control">{{ App\Page::find($page->id)->meta_keywords }}</textarea>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="translation">
                        <div class="form-group">
                            <label for="body-en">{{ trans('messages.title') }}</label>
                            <input type="text" name="en[title]" id="title-en" value="{{ $translation->title }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="body-en">{{ trans('messages.body') }}</label>
                            <textarea name="en[body]" id="body-en" cols="30" rows="20"
                                      class="form-control cke-editor">{{ $translation->body }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_description_en">{{ trans('messages.meta_description') }}</label>
                            <textarea name="en[meta_description]" id="meta_description_en" cols="30" rows="20"
                                      class="form-control">{{ $translation->meta_description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_keywords_en">{{ trans('messages.meta_keywords') }}</label>
                            <textarea name="en[meta_keywords]" id="meta_keywords_en" cols="30" rows="10"
                                      class="form-control">{{ $translation->meta_keywords }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ trans('messages.edit') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop