@extends('adminlte::page')

@section('content')
    @if(count($errors) > 0)
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.create_page') }}
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('page.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content clearfix">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="page_id">{{ trans('messages.parent_page') }}</label>
                            <select id="page_id" name="page_id" class="form-control">
                                <option>{{ trans('messages.please_select') }}</option>
                                @foreach($pages as $page)
                                    <option value="{{ $page->id }}">{{ $page->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="status" value="1">
                                    {{ trans('messages.status') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title">{{ trans('messages.title') }}</label>
                            <input type="text" name="title" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="image">{{ trans('messages.image') }}</label>
                            <input type="file" name="image" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="body">{{ trans('messages.body') }}</label>
                            <textarea name="body" id="body" cols="30" rows="20" class="form-control"> </textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_description">{{ trans('messages.meta_description') }}</label>
                            <textarea name="meta_description" id="meta_description" cols="30" rows="20"
                                      class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_keywords">{{ trans('messages.meta_keywords') }}</label>
                            <textarea name="meta_keywords" id="meta_keywords" cols="30" rows="10"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="translation">

                        <div class="form-group">
                            <label for="title-en">{{ trans('messages.title') }}</label>
                            <input type="text" name="en[title]" id="title-en"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="body-en">{{ trans('messages.body') }}</label>
                            <textarea name="en[body]" id="body-en" cols="30" rows="20"
                                      class="form-control cke-editor"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_description_en">{{ trans('messages.meta_description') }}</label>
                            <textarea name="en[meta_description]" id="meta_description_en" cols="30" rows="20"
                                      class="form-control cke-editor"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="meta_keywords_en">{{ trans('messages.meta_keywords') }}</label>
                            <textarea name="en[meta_keywords]" id="meta_keywords_en" cols="30" rows="10"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ trans('messages.create') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop