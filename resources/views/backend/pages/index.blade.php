@extends('adminlte::page')

@section('content')
    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.pages') }}</h2>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <form action="{{ route('page.index') }}" method="get">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" name="search"/>
                            <span class="input-group-btn">
                      <input type="submit" class="btn btn-flat" value="{{ trans('messages.search') }}"/>

                    </span>
                        </div>
                    </form>
                </div>
            </div>
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.body') }}</th>
                    <th>{{ trans('messages.meta_description') }}</th>
                    <th>{{ trans('messages.meta_keywords') }}</th>
                    <th>{{ trans('messages.subpages') }}</th>
                    <th>{{ trans('messages.to_edit') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($pages->count())
                    @foreach($pages as $page)
                        <tr>
                            <td>
                                {{ $page->title }}
                            </td>
                            <td>
                                {!! str_limit(strip_tags($page->body), 30) !!}
                            </td>
                            <td>
                                {{ $page->meta_description }}
                            </td>
                            <td>
                                {{ $page->meta_keywords }}
                            </td>
                            <td>
                                @foreach($page->child as $pageItem)
                                    {{$pageItem->title}}@if (!$loop->last), @endif
                                @endforeach
                            </td>
                            <td>
                                <a href="{{ route('page.edit', ['id'=> $page->id] ) }}" class="btn btn-success">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('page.delete', ['id'=> $page->id] ) }}" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_active_pages') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {{ $pages->links() }}
        </div>
    </div>
@stop
