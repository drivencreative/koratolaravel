@extends('adminlte::page')

@section('content')
    @if(count($errors) > 0)
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.create_setting') }}
        </div>

        <div class="panel-body">
            <form action="{{ route('setting.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="key">{{ trans('messages.key') }}</label>
                    <input type="text" name="key" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="display_name">{{ trans('messages.display_name') }}</label>
                    <input type="text" name="display_name" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="value">{{ trans('messages.value') }}</label>
                    <input type="text" name="value" class="form-control value" />
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"
                                   name="file_upload"
                                   class="file-upload-switch"
                            >
                            {{ trans('messages.file_upload') }}
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="details">{{ trans('messages.details') }}</label>
                    <input type="text" name="details" class="form-control" />
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ trans('messages.create') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop