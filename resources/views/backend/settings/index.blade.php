@extends('adminlte::page')

@section('content')
    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.settings') }}</h2>
        </div>
        <div class="box-body">
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{ trans('messages.key') }}</th>
                    <th>{{ trans('messages.display_name') }}</th>
                    <th>{{ trans('messages.value') }}</th>
                    <th>{{ trans('messages.details') }}</th>
                    <th>{{ trans('messages.to_edit') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($settings->count())
                    @foreach($settings as $setting)
                        <tr>
                            <td>
                                {{ $setting->key }}
                            </td>
                            <td>
                                {{ $setting->display_name }}
                            </td>
                            <td>
                                {{ $setting->value }}
                            </td>
                            <td>
                                {{ $setting->details }}
                            </td>
                            <td>
                                <a href="{{ route('setting.edit', ['id'=> $setting->id] ) }}" class="btn btn-success">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('setting.delete', ['id'=> $setting->id] ) }}" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_active_settings') }}</td>
                    </tr>
                @endif
                <tfoot>
                    <tr>
                        <th colspan="5">
                            <a href="{{ route('setting.create') }}" class="btn btn-primary">
                                <i class="fa fa-file-o"></i> {{ trans('messages.new_m') }}
                            </a>
                        </th>
                    </tr>
                </tfoot>
                </tbody>
            </table>
            {{ $settings->links() }}
        </div>

    </div>
@stop
