@extends('adminlte::page')

@section('content')

    @include('backend.includes.errors')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.edit_settings') }}
        </div>
        <div class="panel-body">
            <form action="{{ route('setting.update', ['id'=> $setting->id] ) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="key">{{ trans('messages.key') }}</label>
                    <input type="text" name="key" value="{{ $setting->key }}" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="display_name">{{ trans('messages.display_name') }}</label>
                    <input type="text" name="display_name" value="{{ $setting->display_name }}" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="value">{{ trans('messages.value') }}</label>
                    <input type="text" name="value" value="{{ $setting->value }}" class="form-control value" />
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"
                                   name="file_upload"
                                   value="{{ file_exists($setting->value) }}"
                                   class="file-upload-switch"
                                   @if(file_exists($setting->value))
                                   checked
                                    @endif
                            >
                            {{ trans('messages.file_upload') }}
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="details">{{ trans('messages.details') }}</label>
                    <input type="text" name="details" value="{{ $setting->details }}" class="form-control" />
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ trans('messages.edit') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop