@extends('adminlte::page')

@section('content')

    @include('backend.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.edit_product') }}
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('product.update', ['id'=> $product->id] ) }}" method="post"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="series_id">{{ trans('messages.series') }}</label>
                            <select id="series_id" value="{{ $product->series_id }}" name="series_id" class="form-control">
                                @foreach($series as $seriesItem)
                                    <option value="{{ $seriesItem->id }}">{{ $seriesItem->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_new" value="{{ $product->is_new }}"
                                           @if($product->is_new)
                                           checked
                                            @endif
                                    >
                                    {{ trans('messages.is_new') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input type="text" name="name" value="{{ $product->name }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="image">{{ trans('messages.image') }}</label>
                            <input type="file" name="image" value="{{ $product->image }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="description">{{ trans('messages.description') }}</label>
                            <textarea name="description" id="body" cols="30" rows="20"
                                      class="form-control">{{ $product->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="specification">{{ trans('messages.specification') }}</label>
                            <textarea name="specification" id="specification" cols="30" rows="20"
                                      class="form-control">{{ $product->specification }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="price_eu">{{ trans('messages.price_eu') }}</label>
                            <input type="text" name="price_eu" value="{{ $product->price_eu }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="price_rsd">{{ trans('messages.price_rsd') }}</label>
                            <input type="text" name="price_rsd" value="{{ $product->price_rsd }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="price_description">{{ trans('messages.price_description') }}</label>
                            <textarea name="price_description" id="price_description" cols="30" rows="10"
                                      class="form-control">{{ $product->price_description }}</textarea>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="translation">
                        <div class="form-group">
                            <label for="en[name]">{{ trans('messages.name') }}</label>
                            <input type="text" name="en[name]" id="name-en" value="{{ $translation->name }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="en[description]">{{ trans('messages.description') }}</label>
                            <textarea name="en[description]" id="body-en" cols="30" rows="20"
                                      class="form-control">{{ $translation->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="specification">{{ trans('messages.specification') }}</label>
                            <textarea name="en[specification]" id="specification-en" cols="30" rows="20"
                                      class="form-control">{{ $translation->specification }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="en[price_description]">{{ trans('messages.price_description') }}</label>
                            <textarea name="en[price_description]" cols="30" rows="10"
                                      class="form-control">{{ $translation->price_description }}</textarea>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ trans('messages.edit') }}</button>
                </div>
        </form>
    </div>
    </div>
@stop