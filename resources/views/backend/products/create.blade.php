@extends('adminlte::page')

@section('content')
    @if(count($errors) > 0)
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.create_product') }}
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="series_id">{{ trans('messages.series') }}</label>
                            <select id="series_id" name="series_id" class="form-control">
                                <option>{{ trans('messages.please_select') }}</option>
                                @foreach($series as $seriesItem)
                                    <option value="{{ $seriesItem->id }}">{{ $seriesItem->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_new" value="1">
                                    {{ trans('messages.is_new') }}
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input type="text" name="name" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="image">{{ trans('messages.image') }}</label>
                            <input type="file" name="image" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="description">{{ trans('messages.description') }}</label>
                            <textarea name="description" id="body" cols="30" rows="20"
                                      class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="specification">{{ trans('messages.specification') }}</label>
                            <textarea name="specification" id="specification" cols="30" rows="20"
                                      class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="price_eu">{{ trans('messages.price_eu') }}</label>
                            <input type="text" name="price_eu" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="price_rsd">{{ trans('messages.price_rsd') }}</label>
                            <input type="text" name="price_rsd" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="price_description">{{ trans('messages.price_description') }}</label>
                            <textarea name="price_description" id="price_description" cols="30" rows="10"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="translation">
                        <div class="form-group">
                            <label for="en[name]">{{ trans('messages.name') }}</label>
                            <input type="text" name="en[name]" id="name-en"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="en[description]">{{ trans('messages.description') }}</label>
                            <textarea name="en[description]" id="body-en" cols="30" rows="20"
                                      class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="specification">{{ trans('messages.specification') }}</label>
                            <textarea name="en[specification]" id="specification-en" cols="30" rows="20"
                                      class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="en[price_description]">{{ trans('messages.price_description') }}</label>
                            <textarea name="en[price_description]" cols="30" rows="10"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ trans('messages.create') }}</button>
                </div>
            </form>
        </div>
    </div>
@stop