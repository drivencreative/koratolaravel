@extends('adminlte::page')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.products') }}</h2>
        </div>
        <div class="box-body">
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{ trans('messages.image') }}</th>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.body') }}</th>
                    <th>{{ trans('messages.price_eu') }}</th>
                    <th>{{ trans('messages.price_rsd') }}</th>
                    <th>{{ trans('messages.to_restore') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($products->count())
                    @foreach($products as $product)
                        <tr>
                            <td>
                                @if(file_exists( public_path($product->image)))
                                    <img src="{{ asset($product->image) }}" height="30px">
                                @else
                                    <i class="fa fa-file-image-o fa-lg"></i>
                                @endif
                            </td>
                            <td>
                                {{ $product->name }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($product->description), 30) }}
                            </td>
                            <td>
                                {{ $product->price_eu }}
                            </td>
                            <td>
                                {{ $product->price_rsd }}
                            </td>
                            <td>
                                <a href="{{ route('product.restore', ['id'=> $product->id] ) }}" class="btn btn-undo">
                                    <i class="fa fa-undo"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('product.kill', ['id'=> $product->id] ) }}" class="btn btn-remove">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_trashed_products') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop