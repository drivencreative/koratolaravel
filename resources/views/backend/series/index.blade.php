@extends('adminlte::page')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.series') }}</h2>
        </div>
        <div class="box-body">
            <div class="col-sm-6">
                <form action="{{ route('series.index') }}" method="get">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" name="search"/>
                        <span class="input-group-btn">
                      <input type="submit" class="btn btn-flat" value="{{ trans('messages.search') }}"/>

                    </span>
                    </div>
                </form>
            </div>
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{ trans('messages.image') }}</th>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.description') }}</th>
                    <th>{{ trans('messages.to_edit') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($series->count())
                    @foreach($series as $seriesItem)
                        <tr>
                            <td>
                                @if(file_exists( public_path($seriesItem->image)))
                                    <img src="{{ asset($seriesItem->image) }}" height="30px">
                                @else
                                    <i class="fa fa-file-image-o fa-lg"></i>
                                @endif
                            </td>
                            <td>
                                {{ $seriesItem->name }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($seriesItem->description), 30) }}
                            </td>
                            <td>
                                <a href="{{ route('series.edit', ['id'=> $seriesItem->id] ) }}" class="btn btn-success">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('series.delete', ['id'=> $seriesItem->id] ) }}"
                                   class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_active_series') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
            {{ $series->links() }}
        </div>
    </div>
@stop