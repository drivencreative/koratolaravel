@extends('adminlte::page')

@section('content')

    @include('backend.includes.errors')

    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.edit_series') }}
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('series.update', ['id'=> $series->id] ) }}" method="post"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="company_id">{{ trans('messages.company') }}</label>
                            <select id="company_id" value="{{ $series->company_id }}" name="company_id"
                                    class="form-control">
                                @foreach($companies as $company)
                                    <option value="{{ $company->id }}" {{ $series->company->id == $company->id ? 'selected' : '' }}>{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="in_menu"
                                           {{ $series->in_menu ? ' checked="checked"' : '' }}
                                           @if($series->in_menu)
                                           checked
                                            @endif
                                    >
                                    {{ trans('messages.in_menu') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input type="text" name="name" value="{{ $series->name }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="image">{{ trans('messages.image') }}</label>
                            <input type="file" name="image" value="{{ $series->image }}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="body">{{ trans('messages.description') }}</label>
                            <textarea name="description" id="body" cols="30" rows="20"
                                      class="form-control">{{ $series->description }}</textarea>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="translation">
                        <div class="form-group">
                            <label for="en[name]">{{ trans('messages.name') }}</label>
                            <input type="text" name="en[name]" id="name-en" value="{{ $translation->name }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="en[description]">{{ trans('messages.description') }}</label>
                            <textarea name="en[description]" id="body-en" cols="30" rows="20"
                                      class="form-control cke-editor">{{ $translation->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">{{ trans('messages.edit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop