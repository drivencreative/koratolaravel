@extends('adminlte::page')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2>{{ trans('messages.series') }}</h2>
        </div>
        <div class="box-body">
            <table id="data-list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{ trans('messages.image') }}</th>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.description') }}</th>
                    <th>{{ trans('messages.to_restore') }}</th>
                    <th>{{ trans('messages.to_delete') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($series->count())
                    @foreach($series as $seriesItem)
                        <tr>
                            <td>
                                @if(file_exists( public_path($seriesItem->image)))
                                    <img src="{{ asset($seriesItem->image) }}" height="30px">
                                @else
                                    <i class="fa fa-file-image-o fa-lg"></i>
                                @endif
                            </td>
                            <td>
                                {{ $seriesItem->name }}
                            </td>
                            <td>
                                {{ str_limit(strip_tags($seriesItem->description), 30) }}
                            </td>
                            <td>
                                <a href="{{ route('series.restore', ['id'=> $seriesItem->id] ) }}" class="btn btn-undo">
                                    <i class="fa fa-undo"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('series.kill', ['id'=> $seriesItem->id] ) }}" class="btn btn-remove">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">{{ trans('messages.no_trashed_series') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop