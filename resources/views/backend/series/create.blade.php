@extends('adminlte::page')

@section('content')
    @if(count($errors) > 0)
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-danger">
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ trans('messages.create_series') }}
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab"
                   data-toggle="tab">
                    {{ trans('messages.page') }}
                </a>
            </li>
            <li role="presentation">
                <a href="#translation" aria-controls="translation" role="tab" data-toggle="tab">
                    {{ trans('messages.translation') }}
                </a>
            </li>
        </ul>
        <div class="panel-body">
            <form action="{{ route('series.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <label for="company_id">{{ trans('messages.company') }}</label>
                            <select id="company_id" name="company_id" class="form-control">
                                <option>{{ trans('messages.please_select') }}</option>
                                @foreach($companies as $company)
                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="in_menu"/>
                                    {{ trans('messages.in_menu') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">{{ trans('messages.name') }}</label>
                            <input type="text" name="name" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="image">{{ trans('messages.image') }}</label>
                            <input type="file" name="image" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="body">{{ trans('messages.body') }}</label>
                            <textarea name="body" id="body" cols="30" rows="20"
                                      class="form-control cke-editor"></textarea>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="translation">
                        <div class="form-group">
                            <label for="name-en">{{ trans('messages.name') }}</label>
                            <input type="text" name="en[name]" id="name-en"
                                   class="form-control cke-editor"/>
                        </div>
                        <div class="form-group">
                            <label for="description-en">{{ trans('messages.description') }}</label>
                            <textarea name="en[description]" id="description-en" cols="30" rows="20"
                                      class="form-control cke-editor"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">{{ trans('messages.create') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop