@extends('layouts.index')

@section('left')
@stop
@section('content')
    <div class="right_holder">
        <div class="spacer_right">
        </div>
        <div class="description">
            {{ trans('messages.404') }}
        </div>
        <br>
        <br>
        <div class="new_products_big">
            <div class="arial_11_4d" style="padding-left:15px">
                <br><br>
            </div>
        </div>
    </div>
@stop
@section('footer')
    @include('partials.footer')
@stop