Hello {{ $mailer->receiver }},
This is a mailer email for testing purposes! Also, it's the HTML version.

mailer object values:

mailer One: {{ $mailer->mailer_one }}
mailer Two: {{ $mailer->mailer_two }}

Values passed by With method:

testVarOne: {{ $testVarOne }}
testVarOne: {{ $testVarOne }}

Thank You,
{{ $mailer->sender }}