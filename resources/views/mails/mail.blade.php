Hello <i>{{ $mailer->receiver }}</i>,
<p>This is a mailer email for testing purposes! Also, it's the HTML version.</p>

<p><u>mailer object values:</u></p>
<div>
    <p><b>mailer One:</b>&nbsp;{{ $mailer->mailer_one }}</p>
    <p><b>mailer Two:</b>&nbsp;{{ $mailer->mailer_two }}</p>
</div>

<p><u>Values passed by With method:</u></p>

<div>
    <p><b>testVarOne:</b>&nbsp;{{ $testVarOne }}</p>
    <p><b>testVarTwo:</b>&nbsp;{{ $testVarTwo }}</p>
</div>

Thank You,
<br/>
<i>{{ $mailer->sender }}</i>