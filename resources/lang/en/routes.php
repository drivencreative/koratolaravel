<?php
// resources/lang/en/routes.php
return [
    "company" 	=> 	'company/{slug}',
    "product" 	=> 	'product/{slug}',
    "series" 	=> 	'series/{slug}',
    "page" 	=> 	'page/{slug}',
    "index" 	=> 	'/',
];