<?php
// resources/lang/sr/routes.php
return [
    "companies" 	=> 	'kompanija/{slug}',
    "product" 	=> 	'proizvod/{slug}',
    "series" 	=> 	'serija/{slug}',
    "page" 	=> 	'/{slug}',
    "index" 	=> 	'/',
];