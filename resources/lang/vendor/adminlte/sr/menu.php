<?php

return [

    'main_navigation'               => 'MAIN NAVIGATION',
    'blog'                          => 'Blog',
    'pages'                         => 'Pages',
    'account_settings'              => 'PODEŠAVANJA NALOGA',
    'profile'                       => 'Korisnički nalog',
    'change_password'               => 'Promena lozinke',
    'multilevel'                    => 'Multi Level',
    'level_one'                     => 'Level 1',
    'level_two'                     => 'Level 2',
    'level_three'                   => 'Level 3',
    'labels'                        => 'LABELS',
    'Important'                     => 'Important',
    'Warning'                       => 'Warning',
    'Information'                   => 'Information',
];
