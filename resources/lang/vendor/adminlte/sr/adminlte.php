<?php

return [

    'full_name'                   => 'Puno Ime',
    'email'                       => 'Email',
    'password'                    => 'Password',
    'retype_password'             => 'Ponovite password',
    'remember_me'                 => 'Zapamti Me',
    'register'                    => 'Registruj',
    'register_a_new_membership'   => 'Registruj novog člana',
    'i_forgot_my_password'        => 'Zaboravio sam password',
    'i_already_have_a_membership' => 'Već imam nalog',
    'sign_in'                     => 'Loguj me',
    'log_out'                     => 'Izloguj se',
    'toggle_navigation'           => 'Toggle navigation',
    'login_message'               => 'Logovanje na backend',
    'register_message'            => 'Register a new membership',
    'password_reset_message'      => 'Reset Password',
    'reset_password'              => 'Reset Password',
    'send_password_reset_link'    => 'Send Password Reset Link',
];
